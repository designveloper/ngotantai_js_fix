JavaScript is the programming language of HTML and the Web and JavaScript is one of the 3 languages all web developers must learn:

1.  HTML to define the content of web pages

2.  CSS to specify the layout of web pages

3.  JavaScript to program the behavior of web pages

The List what i have to know about JS

1.  ## DATA TYPE


    * In programming, data types is an important concept. There is 7 type of data in JS.
      * Number
        * var length = 16;
      * String
        * var lastName = "Johnson";
      * Boolean
        * var x = 5;
          var y = 5;
          var z = 6;
          (x == y)
          (x == z)
      * Object
        * var x = {firstName:"John", lastName:"Doe"};
      * Null
        * var person = null;
      * Undefined
      * Symbol (ES6)

2.  ## OBJECT


    * In JavaScript, objects are king. If you understand objects, you understand JavaScript.
    * Objects are mutable: They are addressed by reference, not by value

      * Booleans can be objects (if defined with the new keyword)
      * Numbers can be objects (if defined with the new keyword)
      * Strings can be objects (if defined with the new keyword)
      * Dates are always objects
      * Maths are always objects
      * Arrays are always objects
      * Functions are always objects
      * Objects are always objects

      - JavaScript defines 5 types of primitive data types:
        * String
        * Number
        * Boolean
        * Null
        * Undefined
      - There are different ways to create new objects:
        * Define and create a single object, using an object literal.
        * Define and create a single object, with the keyword new.
        * Define an object constructor, and then create objects of the constructed type.

    - Ex :

          var person = {
                        firstName:"John",
                        lastName:"Doe",
                        age:50,
                        eyeColor:"blue"
                        };

3.  ## SCOPE


    * "Scope" is just a technical term for the parts of your code that have access to a variable.

           JavaScript has two scopes -  global and local. Any variable declared outside of a function belongs to the global scope, and is therefore accessible from anywhere in your code. Each function has its own scope, and any variable declared within that function is only accessible from that function and any nested functions.
               * Related : Variable Scope, Hoisting

    - Global Scope
    - Local Scope

      * The global scope is the scope that contains, and is visible in, all other scopes.
      * In client-side JavaScript, the global scope is generally the web page inside which all the code is being executed.
      * Ex:

            var global = 10; // global varriable
            function fun(){
                var local  = 3; // local varriable
                function subfun(){
                    var local = 19;  // local varriable
                }
            }

    - Note
      * All declarations, both functions and variables, are hoisted to the top of the containing scope, before any part of your code is executed.
      * Functions are hoisted first, and then variables.
      * Function declarations have priority over variable declarations, but not over variable assignments.

4.  ## VAR - LET - CONST: function scope and block scope

    * VAR

      The JavaScript variables statement is used to declare a variable and, optionally, we can initialize the value of that variable.

          ex:

            function nodeSimplified(){
            var a =10;
            console.log(a);  // output 10
            if(true){
            var a=20;
            console.log(a); // output 20
            }
            console.log(a);  // output 20
          }

    * LET

      The let statement declares a local variable in a block scope. It is similar to var, in that we can optionally initialize the variable.

            ex:

              function nodeSimplified(){
              let a =10;
              console.log(a);  // output 10
              if(true){
              let a=20;
              console.log(a); // output 20
              }
              console.log(a);  // output 10
            }

    * CONST

      The const statement values can be assigned once and they cannot be reassigned. The scope of const statement works similar to let statements.

      * when we try to reassign the const variable ===>> Error Message : Uncaught TypeError: Assignment to constant variable.


               ex:

                function nodeSimplified(){
                const MY_VARIABLE =10;
                console.log(MY_VARIABLE);  //output 10
              }

5.  ## HOISTING: function and variable

    * JavaScript Declarations are Hoisted
    * Hoisting is a JavaScript mechanism where variables and function declarations are moved to the top of their scope before code execution.

      * Hoisting variables

              x = 5; // Assign 5 to x
              elem = document.getElementById("demo"); // Find an element
              elem.innerHTML = x; // Display x in the element
              var x; // Declare x

      * Hoisting function

            hoisted(); // Output: "This function has been hoisted.
            function hoisted() {
            console.log('This function has been hoisted.');
                };

6.  ## CLOSURE

    * A closure is an inner function that has access to the outer (enclosing) function's variables—scope chain.

7.  ## THIS
    \*
